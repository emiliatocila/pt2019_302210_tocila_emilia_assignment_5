import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.*;

public class Main {

	public static void main(String[] args) {
		MonitoredDataProcessing mdp = new MonitoredDataProcessing();
		mdp.readData();
		try {
			mdp.processDates();
		} catch (Exception e) {
			e.printStackTrace();
		}
		mdp.nrDays();
		mdp.nrOfAppearancesActivities();
		mdp.nrOfAppearancesEachDayActivities();
		mdp.durationForEachLine();
		mdp.durationForEachActivityPeriod();
		mdp.activitiesLessThan5();
	}
}

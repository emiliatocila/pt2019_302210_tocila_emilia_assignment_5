import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MonitoredData {
	private String startTime;
	private String endTime;
	private String activity;

	public MonitoredData(String startTime, String endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		Matcher m = Pattern.compile("(\\S+)").matcher(activity);
		if(m.find()) 
			this.activity = m.group(1);
	}

	public int getDayOfMonth() {
		String s = "";
		Matcher m = Pattern.compile("(\\d+)-(\\d+)-(\\d+)").matcher(this.getStartTime());
		if(m.find()) 
			s += m.group(3);
		return Integer.parseInt(s);
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return "MonitoredData [startTime=" + startTime + ", endTime=" + endTime + ", activity=" + activity + "]";
	}

}

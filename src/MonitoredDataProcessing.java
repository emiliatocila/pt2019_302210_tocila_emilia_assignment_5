import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MonitoredDataProcessing {
	private ArrayList<Date> dateStart;
	private ArrayList<Date> dateEnd;
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat durationFormat;
	private List<MonitoredData> monitoredData;
	private String filePath ="Activities.txt";
	private int nrOfDays = 0;
	private ArrayList<String> days;
	private Map<String, Long> nrOfAppearances;
	private Map<Integer, Map<String, Long>> nrOfAppearancesEachDay;
	private Map<String, String> durationEachActivityPeriod;
	private Map<String, List<Boolean>> activitiesLessThan5;
	private List<String> activitiesLessThan590;

	public MonitoredDataProcessing() {
		dateStart = new ArrayList<Date>();
		dateEnd = new ArrayList<Date>();
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		durationFormat = new SimpleDateFormat("HH:mm:ss");
		monitoredData = new ArrayList<MonitoredData>();
		days = new ArrayList<String>();
		nrOfAppearances = new HashMap<String, Long>();
		nrOfAppearancesEachDay = new HashMap<Integer, Map<String, Long>>();
		durationEachActivityPeriod = new HashMap<String, String>();
		activitiesLessThan5 = new HashMap<String, List<Boolean>>();
		activitiesLessThan590 = new ArrayList<String>();
	}

	public void processDates() throws Exception {
		for(MonitoredData md : monitoredData) {
			dateStart.add(dateFormat.parse(md.getStartTime()));
			dateEnd.add(dateFormat.parse(md.getEndTime()));
		}
	}

	public void readData() {
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
			monitoredData = stream.map(parts -> parts.split("\\s{2}")).map(md -> new MonitoredData(md[0], md[1], md[2])).collect(Collectors.toList());
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public String getDateString(Date dateToString) {
		String s = "";
		String date = dateFormat.format(dateToString);
		Matcher m = Pattern.compile("(\\d+)-(\\d+)-(\\d+)").matcher(date);
		if(m.find()) 
			s += m.group(1) + "-" + m.group(2) + "-" + m.group(3);
		return s;
	}

	public Date computeDuration(String startTime, String endTime) {
		String s = "";
		Date dateS = null;
		Date dateE = null;
		try {
			dateS = dateFormat.parse(startTime);
			dateE = dateFormat.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long diff = dateE.getTime() - dateS.getTime();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		Date duration = null;
		try {
			duration = durationFormat.parse(diffHours + ":" + diffMinutes + ":" + diffSeconds);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return duration;
	}
	
	@SuppressWarnings("deprecation")
	public String convertDateToString(Date date) {
		String s = "";
		long diffSeconds = date.getSeconds();
		long diffMinutes = date.getMinutes();
		long diffHours = date.getHours();
		s += diffHours + ":";
		s += diffMinutes + ":";
		s += diffSeconds;
		return s;
	}
	
	@SuppressWarnings("deprecation")
	public String addDurations(String duration1, String duration2) {
		String s = "";
		int hours1 = 0;
		int hours2 = 0;
		int minutes1 = 0;
		int minutes2 = 0;
		int seconds1 = 0;
		int seconds2 = 0;
		Matcher m = Pattern.compile("(\\d+):(\\d+):(\\d+)").matcher(duration1);
		if(m.find()) {
			hours1 = Integer.parseInt(m.group(1));
			minutes1 = Integer.parseInt(m.group(2));
			seconds1 = Integer.parseInt(m.group(3));
		}
		m = Pattern.compile("(\\d+):(\\d+):(\\d+)").matcher(duration2);
		if(m.find()) {
			hours2 = Integer.parseInt(m.group(1));
			minutes2 = Integer.parseInt(m.group(2));
			seconds2 = Integer.parseInt(m.group(3));
		}
		long add = (hours1 + hours2) * 3600000  + (minutes1 + minutes2) * 60000 + (seconds1 + seconds2) * 1000;
		long addSeconds = add / 1000 % 60;
		long addMinutes = add / (60 * 1000) % 60;
		long addHours = add / (60 * 60 * 1000);
		s += addHours + ":";
		s += addMinutes + ":";
		s += addSeconds;

		return s;
	}
	
	public boolean lessThan5(String duration) {
		int hours = 0;
		int minutes = 0;
		Matcher m = Pattern.compile("(\\d+):(\\d+):(\\d+)").matcher(duration);
		if(m.find()) {
			hours = Integer.parseInt(m.group(1));
			minutes = Integer.parseInt(m.group(2));
		}
		if(hours == 0 && minutes < 5)
			return true;
		return false;
	}
	
	public int getNrOfActivitiesLessThan5(List<Boolean> lessThan5) {
		int nrOfActivitiesLessThan5 = 0;
		for(Boolean bool : lessThan5)
			if(bool == true)
				nrOfActivitiesLessThan5++;
		return nrOfActivitiesLessThan5;
	}

	public void nrDays() {
		for(Date date : dateStart) {
			days.add(this.getDateString(date));
		}
		for(Date date : dateEnd)
			days.add(this.getDateString(date));
		nrOfDays = days.stream().distinct().collect(Collectors.toList()).size();
		System.out.println("Number of days monitored: " + nrOfDays);
	}

	public void nrOfAppearancesActivities() {
		System.out.println("Number of appearances for each activity: ");
		nrOfAppearances = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		System.out.println(nrOfAppearances);
	}

	public void nrOfAppearancesEachDayActivities() {
		System.out.println("Number of appearances for each activity each day: ");
		nrOfAppearancesEachDay = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getDayOfMonth, Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		for(Integer day : nrOfAppearancesEachDay.keySet()) {
			System.out.print("Day " + day + ": ");
			System.out.println(nrOfAppearancesEachDay.get(day));
		}
	}

	public void durationForEachLine() {
		System.out.println("Duration for each entry: ");
		monitoredData.stream().map(md -> this.convertDateToString(computeDuration(md.getStartTime(), md.getEndTime()))).forEach(System.out::println);
	}
	
	public void durationForEachActivityPeriod() {
		System.out.println("Duration for each activity over the monitoring period: ");
		durationEachActivityPeriod = monitoredData.stream().collect(Collectors.toMap(MonitoredData::getActivity, md -> convertDateToString(computeDuration(md.getStartTime(), md.getEndTime())), (val1, val2) -> addDurations(val1, val2)));
		System.out.println(durationEachActivityPeriod);
	}
	
	public void activitiesLessThan5() {
		System.out.println("Activities that have 90% of the monitoring records with duration less than 5 minutes: ");
		activitiesLessThan5 = monitoredData.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.mapping(md -> this.lessThan5(convertDateToString(computeDuration(md.getStartTime(), md.getEndTime()))), Collectors.toList())));
		activitiesLessThan590 = activitiesLessThan5.keySet().stream().filter(hm -> getNrOfActivitiesLessThan5(activitiesLessThan5.get(hm)) >= activitiesLessThan5.get(hm).size() * 0.9 - 2 && getNrOfActivitiesLessThan5(activitiesLessThan5.get(hm)) <= activitiesLessThan5.get(hm).size() * 0.9 + 2).collect(Collectors.toList());
		System.out.println(activitiesLessThan590);
	}

}